﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Cube::Awake()
extern void Cube_Awake_mD02462AC1F0338F62FBA74B309A43513D7A0D6C5 (void);
// 0x00000002 System.Void Cube::Update()
extern void Cube_Update_m92E30DF8DDE75DE867ED92B41FAE863601717D7E (void);
// 0x00000003 System.Void Cube::OnDeletePos()
extern void Cube_OnDeletePos_m70DA1DEB0C5C5CC15568F3207BA20250C225BE39 (void);
// 0x00000004 System.Void Cube::SetUp(System.Single,System.Single)
extern void Cube_SetUp_m43DDAEC0EFC210E0AB73A4B000292F6F5926DC78 (void);
// 0x00000005 System.Void Cube::.ctor()
extern void Cube__ctor_m6140AE68D7A41B6B2420D7C9BE28F59E2EE58292 (void);
// 0x00000006 System.Single InputManager::get_Speed()
extern void InputManager_get_Speed_m773AC1A6332E8715D10489E4E1FEF8E8EEAD9383 (void);
// 0x00000007 System.Single InputManager::get_TimeBetweenSpawn()
extern void InputManager_get_TimeBetweenSpawn_mF911079D55E6F43265724F58C8DEEC226975E9E2 (void);
// 0x00000008 System.Single InputManager::get_Distance()
extern void InputManager_get_Distance_mD73930C42CAD3C71ED8DEEA5EC8ECC54B5D6B69E (void);
// 0x00000009 System.Void InputManager::.ctor()
extern void InputManager__ctor_m52D2F3B9FA0D50C52BCC92486F49B300E9334C2A (void);
// 0x0000000A System.Void Spawner::Start()
extern void Spawner_Start_m8BB68F2141CF386C08091E93308438F56B08EE73 (void);
// 0x0000000B System.Collections.IEnumerator Spawner::Spawn()
extern void Spawner_Spawn_m0A6552DF576E8E58EDD541DC97DE227A0C55854A (void);
// 0x0000000C System.Void Spawner::.ctor()
extern void Spawner__ctor_mBF592E8E9B5682687D8C28E73A64BF29B6BF2088 (void);
// 0x0000000D System.Void Spawner/<Spawn>d__3::.ctor(System.Int32)
extern void U3CSpawnU3Ed__3__ctor_m8F0D6E28A2C7A1BD5F1EDD739DC85D2BAF60AE66 (void);
// 0x0000000E System.Void Spawner/<Spawn>d__3::System.IDisposable.Dispose()
extern void U3CSpawnU3Ed__3_System_IDisposable_Dispose_m84F694B476080E17062190D17B825BC0D1880FAD (void);
// 0x0000000F System.Boolean Spawner/<Spawn>d__3::MoveNext()
extern void U3CSpawnU3Ed__3_MoveNext_m7F4E8DF92E5E6CCCB0B7B08A7E38F4CDD83CC1B6 (void);
// 0x00000010 System.Object Spawner/<Spawn>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpawnU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m87317C2487EC57C2510B7C01A0B2FF34E2DF8622 (void);
// 0x00000011 System.Void Spawner/<Spawn>d__3::System.Collections.IEnumerator.Reset()
extern void U3CSpawnU3Ed__3_System_Collections_IEnumerator_Reset_mC2830591496A04EEA2ED6433CD7F65E0F1ED7B39 (void);
// 0x00000012 System.Object Spawner/<Spawn>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CSpawnU3Ed__3_System_Collections_IEnumerator_get_Current_m5C428AB740D587BC5F996F638017E7C601F36B29 (void);
static Il2CppMethodPointer s_methodPointers[18] = 
{
	Cube_Awake_mD02462AC1F0338F62FBA74B309A43513D7A0D6C5,
	Cube_Update_m92E30DF8DDE75DE867ED92B41FAE863601717D7E,
	Cube_OnDeletePos_m70DA1DEB0C5C5CC15568F3207BA20250C225BE39,
	Cube_SetUp_m43DDAEC0EFC210E0AB73A4B000292F6F5926DC78,
	Cube__ctor_m6140AE68D7A41B6B2420D7C9BE28F59E2EE58292,
	InputManager_get_Speed_m773AC1A6332E8715D10489E4E1FEF8E8EEAD9383,
	InputManager_get_TimeBetweenSpawn_mF911079D55E6F43265724F58C8DEEC226975E9E2,
	InputManager_get_Distance_mD73930C42CAD3C71ED8DEEA5EC8ECC54B5D6B69E,
	InputManager__ctor_m52D2F3B9FA0D50C52BCC92486F49B300E9334C2A,
	Spawner_Start_m8BB68F2141CF386C08091E93308438F56B08EE73,
	Spawner_Spawn_m0A6552DF576E8E58EDD541DC97DE227A0C55854A,
	Spawner__ctor_mBF592E8E9B5682687D8C28E73A64BF29B6BF2088,
	U3CSpawnU3Ed__3__ctor_m8F0D6E28A2C7A1BD5F1EDD739DC85D2BAF60AE66,
	U3CSpawnU3Ed__3_System_IDisposable_Dispose_m84F694B476080E17062190D17B825BC0D1880FAD,
	U3CSpawnU3Ed__3_MoveNext_m7F4E8DF92E5E6CCCB0B7B08A7E38F4CDD83CC1B6,
	U3CSpawnU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m87317C2487EC57C2510B7C01A0B2FF34E2DF8622,
	U3CSpawnU3Ed__3_System_Collections_IEnumerator_Reset_mC2830591496A04EEA2ED6433CD7F65E0F1ED7B39,
	U3CSpawnU3Ed__3_System_Collections_IEnumerator_get_Current_m5C428AB740D587BC5F996F638017E7C601F36B29,
};
static const int32_t s_InvokerIndices[18] = 
{
	3372,
	3372,
	3372,
	1547,
	3372,
	3318,
	3318,
	3318,
	3372,
	3372,
	3282,
	3372,
	2723,
	3372,
	3313,
	3282,
	3372,
	3282,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	18,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
