using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Spawner : MonoBehaviour
{
    [SerializeField] Cube _template;

    [SerializeField]
    private InputManager _inputManager;


   
    private void Start()
    {
        StartCoroutine(Spawn());
    }
    

    IEnumerator Spawn()
    {
        
        while (true)
        {

            var cubeInstance = Instantiate(_template, transform.position, Quaternion.identity);
            cubeInstance.SetUp(_inputManager.Distance,_inputManager.Speed);
            yield return new WaitForSeconds(_inputManager.TimeBetweenSpawn);
        }
    }
}
