using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;



public class Cube : MonoBehaviour
{
    [SerializeField] private Vector3 _endPos = new Vector3(0f, 0f, 0f);
    [SerializeField] private Vector3 _startPos;

    [SerializeField] private float _desiredDuration;
    [SerializeField] private float _passedTime;


   




    private void Awake()
    {
        _passedTime = 0;
        _startPos = transform.position;
       
        
    }
    private void Update()
    {
        _passedTime += Time.deltaTime;
        float percentageComplete = _passedTime / _desiredDuration;
        transform.position = Vector3.Lerp(_startPos, _endPos, percentageComplete);


        OnDeletePos();
        
    }
    private void OnDeletePos()
    {
        if(transform.position == _endPos)
        {
            Destroy(gameObject);
        }
    }
    public void SetUp(float distance, float speed)
    {
        _endPos = new Vector3(0f,0f, distance);
        _desiredDuration = speed;
    }

  
}
