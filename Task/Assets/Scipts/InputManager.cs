using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


public class InputManager : MonoBehaviour
{
    [SerializeField] private TMP_InputField _speed;
    [SerializeField] private TMP_InputField _distance;
    [SerializeField] private TMP_InputField _timeSpawnField;

    public float Speed
    {
        get
        {
            if (float.TryParse(_speed.text, out var speed))
                return speed;
            else
                return 0;
        }
    }
    public float TimeBetweenSpawn
    {
        get
        {
            if (float.TryParse(_timeSpawnField.text, out var time))
                return time;
            else
                return 0;
        }
    }
    public float Distance
    {
        get
        {
            if (float.TryParse(_distance.text, out var distance))
                return distance;
            else
                return 0;
        }
    }

}